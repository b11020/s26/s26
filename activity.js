// Activity s26

/*
 The questions are as follows:
1. What directive is used by Node.js in loading the modules it needs?
-require
2. What Node.js module contains a method for server creation?
-http
3. What is the method of the http object responsible for creating a server using Node.js?
-createServer() method
4. What method of the response object allows us to set status codes and content types?
-Writehead
5. Where will console.log() output its contents when run in Node.js?
-terminal
6. What property of the request object contains the address's endpoint?
-url
*/
// let http = require("http");
// http.createServer(function(request, response){

// 	response.writeHead(200, {"Content-Type":"text/plain"});

// 	response.end("Hello World");
// }).listen(4000);

// console.log("Server running at localhost:4000");
const http = require("http");

const port = 3000;

		http.createServer((request, response) => {
        if(request.url == '/login'){
                response.writeHead(200, {"Content-Type": "text/plain"})
                response.end("Welcome to the login page");
        }
        else{
                response.writeHead(404, {"Content-Type": "text/plain"})
                response.end("I'm sorry the page you are looking for cannot be found.");
        }
}).listen(port);
console.log(`Server now accessible at localhost: ${port}`);















